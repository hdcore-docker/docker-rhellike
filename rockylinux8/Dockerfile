# HDCore rhellike docker images
ARG RHEL_VERSION=8
FROM rockylinux:${RHEL_VERSION}

ARG RHEL_VERSION
ARG IMAGE_VERSION="${RHEL_VERSION}"
ARG IMAGE_NAME="hdcore/docker-rhellike"
ARG IMAGE_TITLE="HDCore docker-rhellike image"
ARG IMAGE_SOURCE="https://gitlab.com/hdcore-docker/docker-rhellike"
ARG IMAGE_DESCRIPTION="Docker image RHEL like based on rockylinux:${RHEL_VERSION}"

# ARG CONTAINER_USER='contusr'
# ARG CONTAINER_GROUP='contgrp'

# Image labels
LABEL "maintainer"="HDCore" \
    "org.opencontainers.image.title"="${IMAGE_TITLE}" \
    "org.opencontainers.image.description"="${IMAGE_DESCRIPTION}" \
    "org.opencontainers.image.authors"="${IMAGE_AUTHORS}" \
    "org.opencontainers.image.licenses"="MIT" \
    "org.opencontainers.image.url"="https://hub.docker.com/r/${IMAGE_NAME}" \
    "org.opencontainers.image.ref.name"="${IMAGE_NAME}:${IMAGE_VERSION}" \
    "org.opencontainers.image.source"="${IMAGE_SOURCE}"

ENV TZ="Europe/Brussels"

# Copy scripts and add to the path
COPY ./scripts/* /usr/local/bin/hdcore/
RUN chmod +x /usr/local/bin/hdcore/*.sh
ENV PATH="/usr/local/bin/hdcore:${PATH}"

# Add proxy on build time
ARG http_proxy
ARG HTTP_PROXY
RUN set-proxy.sh

# Install basic tools
RUN dnf -y install wget nano git openssl openssh-clients net-tools telnet mailx zip \
    && dnf clean all \
    && rm -rf /var/cache/dnf

# Add certificates on build time
COPY ./certificates/* /certificates/
ARG CACERT_FILE_ROOT
RUN set-cacertificates.sh

# Install Oracle Client 19
COPY ./rockylinux8/ol8-temp.repo /etc/yum.repos.d/ol8-temp.repo
RUN curl https://yum.oracle.com/RPM-GPG-KEY-oracle-ol8 -o /etc/pki/rpm-gpg/RPM-GPG-KEY-oracle \
    && curl https://yum.oracle.com/RPM-GPG-KEY-oracle-ol8 -o /etc/pki/rpm-gpg/RPM-GPG-KEY-oracle \
    && gpg --quiet --with-fingerprint /etc/pki/rpm-gpg/RPM-GPG-KEY-oracle \
    && dnf install -y --setopt=tsflags=nodocs oracle-release-el8 \
    && dnf clean all \
    && rm -rf /var/cache/dnf
RUN dnf install -y --setopt=tsflags=nodocs oracle-instantclient19.10-basic.x86_64 oracle-instantclient19.10-sqlplus.x86_64 oracle-instantclient19.10-devel.x86_64 oracle-instantclient19.10-tools.x86_64 \
    && dnf clean all \
    && rm -rf /var/cache/dnf
RUN mkdir /etc/yum.repos.d/disabled/ \
    && mv /etc/yum.repos.d/*ol8* /etc/yum.repos.d/disabled/
ENV ORACLE_HOME /usr/lib/oracle/19.10/client64
ENV TNS_ADMIN /usr/lib/oracle/19.10/client64/network/admin/
ENV NLS_LANG American_America.UTF8
ENV LD_LIBRARY_PATH=$ORACLE_HOME/lib

# Install Python 3.8.6 with pylint, yamllint and flake
RUN dnf install -y python38 \
    && pip3 install --upgrade pip setuptools cx_Oracle \
    && pip3 install pylint yamllint flake8 pytest \
    && dnf clean all \
    && rm -rf /var/cache/dnf

# Workdir
WORKDIR /workdir

# Entrypoint and command
ENTRYPOINT [ "docker-entrypoint.sh" ]
CMD ["/bin/bash"]
