# HDCore - docker-rhellike

## Introduction

This is a big container image that contains a RHEL-like image with a python 3.8 / 3.9 environment with Oracle Client 19 / 21.

## Image usage

- Run shell:

```bash
docker run -it --rm -v /path/to/workdir:/workdir hdcore/docker-rhellike:<version>
```

### Add http proxy on startup

- Set the http_proxy (lowercase) environment variable

### Add extra CA certificates on startup

- Add the \*.crt to the /certificates/ folder.
- Set the CACERT_FILE_XXXX environment variable who points to local filename
- Set the CACERT_VAR_XXX environment variable with the content of the certificate

## Available tags

- hdcore/docker-rhellike:rockylinux8
- hdcore/docker-rhellike:rockylinux9

## Container Registries

The image is stored on multiple container registries at dockerhub and gitlab:

- Docker Hub:
  - hdcore/docker-rhellike
  - registry.hub.docker.com/hdcore/docker-rhellike
- Gitlab:
  - registry.gitlab.com/hdcore-docker/docker-rhellike

## Building image

Build:

```bash
docker build -f <version>/Dockerfile -t docker-rhellike:<version> .
```

## Running the image

### Default

```bash
docker compose run --rm local-rockylinux8-run /bin/bash
```

### Sleep infinity

```bash
docker compose run --rm local-<version>-run run-infinity.sh
```
